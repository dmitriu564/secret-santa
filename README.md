## Description

Simple secret santa game. To prevent peeking at other's wishes, simple jwt authorization is used.

## Installation

1. Download all the packages
    ```bash
    $ npm install
    ```
2. Set up the database schema
    ```bash
    $ npx mikro-orm schema:create --run
    ```
Important variables like port and jwt key are defined in .env file

## Running the app
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
## Swagger documentation
Swagger documentation is available on the <http://ADDRESS:PORT/api> endpoint(for example, <http://localhost:3000/api>).