import { MikroORM } from '@mikro-orm/core';
import { EntityManager } from '@mikro-orm/sqlite';
import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from './modules/users/users.service';
import { inplace_shuffle } from './utils/shuffle';

@Injectable()
export class AppService {
  constructor(
    private readonly usersService: UsersService,
    private readonly orm: MikroORM,
  ) {}

  async shuffle(): Promise<boolean> {
    const users = await this.usersService.findAll();
    const length = users.length;

    if (length < 3 || length > 500) {
      throw new BadRequestException(
        length < 3 ? 'Insufficient ammount of players' : 'Too many players',
      );
    }

    if (
      users.every((user) => {
        return !user.reciever;
      })
    ) {
      throw new BadRequestException('Already shuffled');
    }

    inplace_shuffle(users);

    users.forEach((user, idx, arr) => {
      user.reciever = arr[(idx + 1) % length];
    });

    this.orm.em.flush();

    return true;
  }

  async removeAll(): Promise<boolean> {
    return this.usersService.removeAll();
  }
}
