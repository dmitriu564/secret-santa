import {
  Entity,
  Formula,
  OneToOne,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';

@Entity()
export class User {
  @PrimaryKey()
  id!: number;

  @Property()
  username!: string;

  @Property()
  name!: string;

  @Property()
  surname!: string;

  @Property({ type: 'text' })
  passwordHash!: string;

  @Property()
  wishes!: string[];

  @OneToOne({ nullable: true })
  santa?: User;

  @OneToOne({ nullable: true, inversedBy: 'santa' })
  reciever?: User;
}
