import { EntityRepository } from '@mikro-orm/core';
import { InjectRepository } from '@mikro-orm/nestjs';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
  ) {}

  async findOneById(id: number): Promise<User | undefined> {
    return this.userRepository.findOne({
      id,
    });
  }

  async findOneByUsername(username: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      username,
    });
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.findAll();
  }

  async getReciever(santaId: number): Promise<Partial<User>> {
    const receiver = await this.userRepository.findOne({
      santa: santaId,
    });
    if (receiver) {
      return {
        name: receiver.name,
        surname: receiver.surname,
        wishes: receiver.wishes,
      };
    }
    throw new BadRequestException("Players weren't shuffled yet");
  }

  async create(user: Omit<User, 'id'>): Promise<User | null> {
    const newUser = this.userRepository.create(user);
    await this.userRepository.persistAndFlush(newUser);
    return newUser;
  }

  async removeAll(): Promise<boolean> {
    try {
      const users = await this.findAll();

      users.forEach((user) => {
        this.userRepository.remove(user);
      });

      await this.userRepository.flush();

      return true;
    } catch (e) {
      throw new InternalServerErrorException('Error when deleting users');
    }
  }
}
