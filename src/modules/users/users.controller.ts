import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthorizedRequest } from 'src/interfaces/authorized-request.interface';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('secret santa')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @Get('receiver')
  @ApiResponse({
    description:
      'If the user is logged in and the list was shuffled, returns info about the user who recieves presents from the currently logged in user',
  })
  @ApiBearerAuth('access-token')
  getReceiver(@Request() { user }: AuthorizedRequest): Promise<Partial<User>> {
    return this.usersService.getReciever(user.id);
  }
}
