import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateUserDTO {
  constructor(partial: Partial<CreateUserDTO>) {
    Object.assign(this, partial);
  }

  @IsNotEmpty()
  @MaxLength(100)
  @ApiProperty({ maxLength: 100 })
  name: string;

  @IsNotEmpty()
  @MaxLength(100)
  @ApiProperty({ maxLength: 100 })
  surname: string;

  @IsNotEmpty()
  @MinLength(8)
  @ApiProperty({ minLength: 8 })
  password: string;

  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(15)
  @ApiProperty({ minLength: 3, maxLength: 15 })
  username: string;

  @IsArray()
  @ArrayMinSize(1)
  @ArrayMaxSize(10)
  @ApiProperty({ type: [String], minLength: 1, maxLength: 10 })
  wishes: string[];
}
