import {
  Controller,
  Post,
  UseGuards,
  Body,
  UseFilters,
  Request,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiHeader,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { RequestValidationFilter } from 'src/filters/request-validation.filter';
import { AuthorizedRequest } from 'src/interfaces/authorized-request.interface';
import { CreateUserDTO } from 'src/modules/users/dto/create-user.dto';
import { DoesUserExist } from './guards/does-user-exist.guard';
import { LocalAuthGuard } from './guards/local.guard';
import { CreateUserToken } from './interfaces/create-user-token.interface';
import { AuthService } from './services/auth.service';

@Controller('auth')
@ApiTags('secret santa')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiParam({
    name: 'username',
    required: true,
    schema: { type: 'string' },
  })
  @ApiParam({
    name: 'password',
    required: true,
    schema: { type: 'string' },
  })
  @ApiResponse({
    description: 'Logs in a user if the credentials are valid',
    type: String,
  })
  async login(
    @Request() { user }: AuthorizedRequest,
  ): Promise<{ token: string }> {
    const token = await this.authService.login(user);
    return { token };
  }

  @UseFilters(RequestValidationFilter)
  @UseGuards(DoesUserExist)
  @ApiCreatedResponse({
    description:
      'Creates a new user if the credentials are valid and username is unique',
    type: CreateUserToken,
  })
  @Post('signup')
  async signUp(@Body() createUserDTO: CreateUserDTO): Promise<CreateUserToken> {
    const data = await this.authService.create(createUserDTO);

    return data;
  }
}
