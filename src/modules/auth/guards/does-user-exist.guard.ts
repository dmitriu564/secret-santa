import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { Observable } from 'rxjs';
import { CreateUserDTO } from 'src/modules/users/dto/create-user.dto';
import { UsersService } from 'src/modules/users/users.service';

@Injectable()
export class DoesUserExist implements CanActivate {
  constructor(private readonly userService: UsersService) {}

  async validateRequest(req): Promise<boolean> {
    const createUser = new CreateUserDTO(req.body);

    const errors = await validate(createUser);

    if (errors.length) {
      throw new BadRequestException(errors);
    }

    const userExists = await this.userService.findOneByUsername(
      req.body.username,
    );
    if (userExists) {
      throw new ForbiddenException('Login already in use');
    }

    return true;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    return this.validateRequest(req);
  }
}
