import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/modules/users/users.service';
import * as argon2 from 'argon2';
import { User } from 'src/modules/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { CreateUserToken } from '../interfaces/create-user-token.interface';
import { CreateUserDTO } from 'src/modules/users/dto/create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  private async comparePassword(enteredPassword, dbPassword): Promise<boolean> {
    return argon2.verify(dbPassword, enteredPassword);
  }

  private async hashPassword(password): Promise<string> {
    return argon2.hash(password);
  }

  private generateToken(user: Partial<User>): Promise<string> {
    return this.jwtService.signAsync(user);
  }

  async validateUser(
    username: string,
    pass: string,
  ): Promise<Omit<User, 'passwordHash'> | null> {
    const user = await this.usersService.findOneByUsername(username);
    if (!user) {
      return null;
    }

    const match = await this.comparePassword(pass, user.passwordHash);
    if (!match) {
      return null;
    }

    const { passwordHash, ...result } = user;
    return result;
  }

  public async login(user: Partial<User>): Promise<string> {
    return this.generateToken(user);
  }

  public async create(userDTO: CreateUserDTO): Promise<CreateUserToken> {
    const { password, ...userData } = userDTO;
    const pass = await this.hashPassword(password);
    const newUser = await this.usersService.create({
      ...userData,
      passwordHash: pass,
    });
    const { passwordHash, ...user } = newUser;
    const token = await this.generateToken(user);
    return { user, token };
  }
}
