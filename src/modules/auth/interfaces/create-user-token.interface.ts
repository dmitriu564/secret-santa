import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/modules/users/entities/user.entity';

export class CreateUserToken {
  @ApiProperty()
  user: Omit<User, 'passwordHash'>;
  @ApiProperty()
  token: string;
}
