import { Controller, Delete, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@Controller()
@ApiTags('secret santa')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('shuffle')
  @ApiResponse({
    description:
      'Shuffles the users. After shuffling each user has someone as their santa and is a santa to some other user',
    type: Boolean,
  })
  shuffle(): Promise<boolean> {
    return this.appService.shuffle();
  }

  @Delete('removeAll')
  @ApiResponse({
    description: 'Removes all users from database',
    type: Boolean,
  })
  removeAll(): Promise<boolean> {
    return this.appService.removeAll();
  }
}
