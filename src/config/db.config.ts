import { User } from '../modules/users/entities/user.entity';

export default {
  entities: [User],
  dbName: 'db.sqlite3',
  type: 'sqlite',
};
