export interface Environment {
  PORT: number;
  JWT_KEY: string;
  TOKEN_EXPIRATION: number;
}
