import { User } from 'src/modules/users/entities/user.entity';

export interface AuthorizedRequest extends Request {
  user: Pick<User, 'id' | 'username' | 'name' | 'surname'>;
}
